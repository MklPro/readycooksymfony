let mapContainer = document.querySelector('#map');
import * as L from 'leaflet';


if (mapContainer !== null) {


    let jsonShop = JSON.parse(mapContainer.getAttribute('data-shop'));

    let locations = [
        ["Carrefour City", 45.776930, 3.079060],
        ["Market Clermont-Ferrand Jaude", 45.774160, 3.081460],
        ["L’ÉPICERIE DU MONDE", 45.773630, 3.080290],
        ["Marché Afric'Asie", 45.7724029, 3.082338],
        ["Auchan Piéton Clermont Blatin", 45.776850, 3.079760],
        ["Auchan Supermarché Blatin - Clermont", 45.768630, 3.090090],
        ["Carrefour City", 45.780430, 3.083460],
    ];


    for (let i = 0; i < jsonShop.length; i++) {
        let ville = [jsonShop[i].name, jsonShop[i].latitude, jsonShop[i].longitude];
        locations.push(ville)

    }
    console.log(locations)

    let map = L.map('map').setView([45.772323758694604, 3.0821117625704573], 13);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYXlvdWIwMTAxMDEiLCJhIjoiY2t6eHdodHh1MDM4bzJxczAxeGx2d2g4byJ9.X7X8SDXx755WaTVNZrsleg', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'your.mapbox.access.token'
    }).addTo(map);


    for (let i = 0; i < locations.length; i++) {

        let myIcon = L.icon({
            iconUrl: 'build/images/marker.png',
            iconSize: [30, 35],
            iconAnchor: [30, 50],
            popupAnchor: [-35, -45],
        });
        let myIcon2 = L.icon({
            iconUrl: 'build/images/map-pin.png',
            iconSize: [35, 35],
            iconAnchor: [30, 50],
            popupAnchor: [-35, -45],
        });

        let marker = L.marker([45.772323758694604, 3.0821117625704573], {icon: myIcon}).addTo(map);
        marker.bindPopup("<h4>Ready & Cook</h4>" + "<p>5 Rue de Gravenoire, 63000 Clermont-Ferrand</p>" + "<a href='https://goo.gl/maps/uRwNnj9xVCBw43zbA'>Venez nous voir</a>");

        let marker2 = new L.marker([locations[i][1], locations[i][2]], {icon: myIcon2}).addTo(map);
        marker2.bindPopup("<h4>" + "</h4>" + "<p>5 Rue de Gravenoire, 63000 Clermont-Ferrand</p>" + "<a href='https://goo.gl/maps/uRwNnj9xVCBw43zbA'>Venez nous voir</a>")
            .addTo(map);
    }
}