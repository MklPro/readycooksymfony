let container = document.querySelector('.ingredients');

if (container !== null) {
        let btn = document.querySelector('[data-confirm]');
        let checkboxes: NodeListOf<HTMLInputElement> = document.querySelectorAll('[data-ingredient-id]');
        btn.setAttribute('disabled', 'true');
        let arrayCheckboxesIds = [];

        btn.addEventListener('click', () => {
                let json = JSON.stringify(arrayCheckboxesIds);
                window.location.href = '/recette_chosed?ingredientId=' + json;
        })

        checkboxes.forEach((elem) => {
                elem.addEventListener('change', function () {
                        if (elem.checked) {
                                arrayCheckboxesIds.push(elem.value);
                        } else {
                                let index = arrayCheckboxesIds.indexOf(elem.value)
                                arrayCheckboxesIds.splice(index);
                        }

                        if (arrayCheckboxesIds.length > 0) {
                                btn.removeAttribute('disabled');
                        } else {
                                btn.setAttribute('disabled', 'true');
                        }
                })
        })
}