<?php

namespace App\Repository;

use App\Entity\Allergy;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Allergy|null find($id, $lockMode = null, $lockVersion = null)
 * @method Allergy|null findOneBy(array $criteria, array $orderBy = null)
 * @method Allergy[]    findAll()
 * @method Allergy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AllergyRepository extends ServiceEntityRepository
{
        public function __construct(ManagerRegistry $registry)
        {
                parent::__construct($registry, Allergy::class);
        }

        public function getByUser(User $user)
        {
                return $this->createQueryBuilder('a')
                            ->innerJoin('a.user', 'u')
                            ->andWhere('u = :user')
                            ->setParameter('user', $user);
        }


        // /**
        //  * @return Allergy[] Returns an array of Allergy objects
        //  */
        /*
        public function findByExampleField($value)
        {
            return $this->createQueryBuilder('a')
                ->andWhere('a.exampleField = :val')
                ->setParameter('val', $value)
                ->orderBy('a.id', 'ASC')
                ->setMaxResults(10)
                ->getQuery()
                ->getResult()
            ;
        }
        */

        /*
        public function findOneBySomeField($value): ?Allergy
        {
            return $this->createQueryBuilder('a')
                ->andWhere('a.exampleField = :val')
                ->setParameter('val', $value)
                ->getQuery()
                ->getOneOrNullResult()
            ;
        }
        */
}
