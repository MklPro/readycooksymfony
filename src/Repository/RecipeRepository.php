<?php

namespace App\Repository;

use App\Entity\Ingredient;
use App\Entity\Recipe;
use App\Entity\SearchData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method Recipe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recipe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recipe[]    findAll()
 * @method Recipe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeRepository extends ServiceEntityRepository
{
        public function __construct(ManagerRegistry $registry)
        {
                parent::__construct($registry, Recipe::class);
        }

        public function getQbAll(): QueryBuilder
        {
                return $this->createQueryBuilder('recipe');
        }

        public function findRecipesByIngredients(array $ingredients)
        {
                return $this->createQueryBuilder('rec')
                            ->innerJoin('rec.ingredients', 'ingredients')
                            ->andWhere('ingredients.id IN (:ids)')
                            ->setParameter('ids', $ingredients)
                            ->groupBy('rec.id')
                            ->having('COUNT(distinct ingredients.id) = :count_ingredient')
                            ->setParameter('count_ingredient', count($ingredients))
                            ->getQuery()
                            ->getResult();
        }

        public function getOtherRecipesByIngredients(array $ingredients, array $recipes): QueryBuilder
        {
                return $this->createQueryBuilder('recipe')
                            ->join('recipe.ingredients', 'i')
                            ->andWhere('i.id in (:ingredientIds)')
                            ->setParameter('ingredientIds', $ingredients)
                            ->andWhere('recipe NOT IN (:recipes)')
                            ->setParameter('recipes', $recipes);
        }

        public function findOtherRecipesByIngredients(array $ingredients, array $recipes): array
        {
                return $this->getOtherRecipesByIngredients($ingredients, $recipes)
                            ->getQuery()
                            ->getResult();
        }

        public function findOtherRecipesByRecipes(array $recipes): array
        {
                return $this->createQueryBuilder('otherRecipes')
                            ->select('otherRecipes')
                            ->andWhere('otherRecipes NOT IN (:recipes)')
                            ->setParameter('recipes', $recipes)
                            ->getQuery()
                            ->getResult();
        }

        public function findByTime(): array
        {
                $time = 30;
                return $this->createQueryBuilder('recipe')
                            ->andWhere('recipe.duration <= (:time)')
                            ->setParameter('time', $time)
                            ->getQuery()
                            ->getResult();
        }

        public function findSearch(SearchData $search)
        {
                $query = $this
                            ->createQueryBuilder('recipe')
                          ->join('recipe.ingredients', 'i')
                          ->join('i.allergies', 'a')
                          ->join('i.diets', 'd')
                ;

                if (!empty($search->q)) {
                        $query
                                    ->andWhere('recipe.name LIKE :q')
                                    ->setParameter('q', "%{$search->q}%");
                }

                if (!empty($search->allergies)) {
                        $query
                                    ->andWhere('a.id NOT IN (:allergies)')
                                    ->setParameter('allergies', $search->allergies);
                }

                if (!empty($search->diets)) {
                        $query
                                    ->andWhere('d IN (:diet)')
                                    ->setParameter('diet', $search->diets);
                }

                if (!empty($search->difficulty)) {
                        $query
                                    ->andWhere('recipe.difficulty IN (:difficulty)')
                                    ->setParameter('difficulty', $search->difficulty);
                }

                if (!empty($search->price)) {
                        $query
                                    ->andWhere('recipe.price IN (:price)')
                                    ->setParameter('price', $search->price);
                }

                if (!empty($search->duration)) {
                        $query
                                    ->andWhere('recipe.duration <= (:duration)')
                                    ->setParameter('duration', $search->duration);
                }

                return $query
                            ->getQuery()
                            ->getResult();
        }
}
