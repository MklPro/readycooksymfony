<?php

namespace App\Form;

use App\Entity\Ingredient;
use App\Entity\Recipe;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecipeType extends AbstractType
{
        public function buildForm(FormBuilderInterface $builder, array $options): void
        {
                $builder
                          ->add('name')
                          ->add('duration')
                          ->add('difficulty')
                          ->add('price')
                          ->add('pathImage')
                          ->add('ingredients', EntityType::class, [
                                    'class' => Ingredient::class,
                                    'choice_label' => 'name',
                                    'expanded' => true,
                                    'multiple' => true,
                          ]);
        }

        public function configureOptions(OptionsResolver $resolver): void
        {
                $resolver->setDefaults([
                          'data_class' => Recipe::class,
                ]);
        }
}
