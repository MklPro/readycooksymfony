<?php

namespace App\Form;

use App\Entity\Allergy;
use App\Entity\Diet;
use App\Entity\SearchData;
use App\Repository\AllergyRepository;
use App\Repository\DietRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SearchType extends AbstractType
{
        private DietRepository $dietRepository;
        private AllergyRepository $allergyRepository;

        /**
         * @param DietRepository $dietRepository
         * @param AllergyRepository $allergyRepository
         */
        public function __construct(DietRepository $dietRepository, AllergyRepository $allergyRepository)
        {
                $this->dietRepository = $dietRepository;
                $this->allergyRepository = $allergyRepository;
        }

        public function buildForm(FormBuilderInterface $builder, array $options): void
        {
                $builder
                          ->add('q', TextType::class, [
                                    'label' => false,
                                    'required' => false,
                                    'data' => '',
                                    'attr' => [
                                              'placeholder' => 'Entrez un nom de recette'
                                    ]
                          ])
                          ->add('allergies', EntityType::class, [
                                    "class" => Allergy::class,
                                    "choice_label" => "name",
                                    "choices" => $this->allergyRepository->findAll(),
                                    'multiple' => true,
                                    'expanded' => true
                          ])
                          ->add('diets', EntityType::class, [
                                    "class" => Diet::class,
                                    "choice_label" => "name",
                                    "choices" => $this->dietRepository->findAll(),
                                    'label' => 'Régimes',
                                    'multiple' => true,
                                    'expanded' => true
                          ])
                          ->add('duration', NumberType::class, [
                                    'label' => 'Durée',
                                    'required' => false,
                          ])
                          ->add('difficulty', ChoiceType::class, [
                                    'choices' => [
                                              'Facile' => "Easy",
                                              'Normal' => "Medium",
                                              'Difficile' => "Hard",
                                    ],
                                    'label' => 'Difficulté',
                                    'multiple' => true,
                                    'expanded' => true,
                                    'required' => false,
                          ])
                          ->add('price', ChoiceType::class, [
                                    'choices' => [
                                              'Bon marché' => "€",
                                              'Modéré' => "€€",
                                              'Supérieur' => "€€€",
                                    ],
                                    'label' => 'Prix',
                                    'multiple' => true,
                                    'expanded' => true,
                                    'required' => false,
                          ]);
        }

        public function configureOptions(OptionsResolver $resolver): void
        {
                $resolver->setDefaults([
                          'data_class' => SearchData::class,
                          'method' => 'GET',
                ]);
        }
}