<?php

namespace App\Form;

use App\Entity\Diet;
use App\Entity\Ingredient;
use App\Repository\IngredientRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DietType extends AbstractType
{
        private IngredientRepository $ingredientRepository;

        /**
         * @param IngredientRepository $ingredientRepository
         */
        public function __construct(IngredientRepository $ingredientRepository)
        {
                $this->ingredientRepository = $ingredientRepository;
        }

        public function buildForm(FormBuilderInterface $builder, array $options): void
        {
                $builder
                            ->add('name')
                            ->add('ingredients', EntityType::class, [
                                        "class" => Ingredient::class,
                                        "choice_label" => "name",
                                        "choices" => $this->ingredientRepository->findAll(),
                                        'expanded' => true,
                                        'multiple' => true
                            ]);
        }

        public function configureOptions(OptionsResolver $resolver): void
        {
                $resolver->setDefaults([
                            'data_class' => Diet::class,
                ]);
        }
}
