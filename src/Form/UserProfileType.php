<?php

namespace App\Form;

use App\Entity\Allergy;
use App\Entity\Diet;
use App\Entity\Recipe;
use App\Entity\Shop;
use App\Entity\User;
use App\Repository\AllergyRepository;
use App\Repository\DietRepository;
use App\Repository\RecipeRepository;
use App\Repository\ShopRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserProfileType extends AbstractType
{
        private DietRepository $dietRepository;
        private AllergyRepository $allergyRepository;
        private ShopRepository $shopRepository;
        private RecipeRepository $recipeRepository;

        /**
         * @param DietRepository $dietRepository
         * @param AllergyRepository $allergyRepository
         * @param ShopRepository $shopRepository
         * @param RecipeRepository $recipeRepository
         */
        public function __construct(DietRepository $dietRepository, AllergyRepository $allergyRepository, ShopRepository $shopRepository, RecipeRepository $recipeRepository)
        {
                $this->dietRepository = $dietRepository;
                $this->allergyRepository = $allergyRepository;
                $this->shopRepository = $shopRepository;
                $this->recipeRepository = $recipeRepository;
        }

        public function buildForm(FormBuilderInterface $builder, array $options): void
        {
                $builder
                            ->add('mail', TextType::class, [
                                        'label' => false,
                                        'required' => false,
                                        'attr' => [
                                                    'placeholder' => 'Entrez votre nouvel email'
                                        ]
                            ])
                            ->add('nickName', TextType::class, [
                                        'label' => false,
                                        'required' => false,
                                        'attr' => [
                                                    'placeholder' => 'Entrez votre nouveau pseudo'
                                        ]
                            ])
                            ->add('accountName', TextType::class, [
                                        'label' => false,
                                        'required' => false,
                                        'attr' => [
                                                    'placeholder' => 'Entrez votre nouveau nom de compte'
                                        ]
                            ])
                            ->add('pathImage', FileType::class, [
                                        'mapped' => false,
                                        'label' => false,
                                        'required' => false
                            ])
                            ->add('allergies', EntityType::class, [
                                        "class" => Allergy::class,
                                        "choice_label" => "name",
                                        "choices" => $this->allergyRepository->findAll(),
                                        'multiple' => true,
                                        'expanded' => true
                            ])
                            ->add('diets', EntityType::class, [
                                        "class" => Diet::class,
                                        "choice_label" => "name",
                                        "choices" => $this->dietRepository->findAll(),
                                        'multiple' => true,
                                        'expanded' => true
                            ])
//                            ->add('shops', EntityType::class, [
//                                        "class" => Shop::class,
//                                        "choice_label" => "name",
//                                        "choices" => $this->shopRepository->findAll(),
//                                        'multiple' => true,
//                                        'expanded' => true
//                            ])
//                            ->add('recipes', EntityType::class, [
//                                        "class" => Recipe::class,
//                                        "choice_label" => "name",
//                                        "choices" => $this->recipeRepository->findAll(),
//                                        'multiple' => true,
//                                        'expanded' => true
//                            ])
                ;

        }

        public function configureOptions(OptionsResolver $resolver): void
        {
                $resolver->setDefaults([
                            'data_class' => User::class,
                ]);
        }
}
