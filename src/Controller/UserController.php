<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserProfileType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Filter\FilterException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class UserController extends AbstractController
{
        private UserRepository $userRepository;
        private EntityManagerInterface $entityManager;

        /**
         * @param UserRepository $userRepository
         * @param EntityManagerInterface $entityManager
         */
        public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
        {
                $this->userRepository = $userRepository;
                $this->entityManager = $entityManager;
        }


        #[Route('/profil', name: 'profil')]
        public function index(Request $request, SluggerInterface $slugger): Response
        {
                /** @var  $user User */
                $user = $this->getUser();

                $form = $this->createForm(UserProfileType::class, $user);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {

                        /** @var  $imageFile UploadedFile */
                        $imageFile = $form->get('pathImage')->getData();

                        if ($imageFile) {
                                $originalFileName = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                                $slugFileName = $slugger->slug($originalFileName);
                                $newFileName = $slugFileName . uniqid() . '.' . $imageFile->guessExtension();

                                try {
                                        $imageFile->move(
                                                    $this->getParameter('image_upload'),
                                                    $newFileName
                                        );
                                } catch (FilterException $e) {

                                }

                                $user->setPathImage('upload/user_avatar/' . $newFileName);
                                $this->entityManager->persist($user);

                        }
                        $this->entityManager->flush();
                }

                return $this->render('user/profile.html.twig', [
                            'controller_name' => 'ProfileController',
                            'userForm' => $form->createView(),
                ]);
        }

}
