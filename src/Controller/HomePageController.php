<?php

namespace App\Controller;

use App\Repository\RecipeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
        private RecipeRepository $recipeRepository;

        /**
         * @param RecipeRepository $recipeRepository
         */
        public function __construct(RecipeRepository $recipeRepository)
        {
                $this->recipeRepository = $recipeRepository;
        }

        #[Route('/', name: 'home')]
        public function index(): Response
        {
                $recipeEntities = $this->recipeRepository->findByTime();

                return $this->render('home_page/home.html.twig', [
                            'recipes' => $recipeEntities,
                            'controller_name' => 'HomePageController',
                ]);
        }
}
