<?php

namespace App\Controller\Admin;

use App\Entity\Recipe;
use App\Form\RecipeType;
use App\Repository\RecipeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/recipe')]
class RecipeController extends AbstractController
{
        #[Route('/', name: 'admin_recipe_index', methods: ['GET'])]
        public function index(RecipeRepository $recipeRepository): Response
        {
                return $this->render('admin/recipe/index.html.twig', [
                            'recipes' => $recipeRepository->findAll(),
                ]);
        }

        #[Route('/new', name: 'admin_recipe_new', methods: ['GET', 'POST'])]
        public function new(Request $request, EntityManagerInterface $entityManager): Response
        {
                $recipe = new Recipe();
                $form = $this->createForm(RecipeType::class, $recipe);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($recipe);
                        $entityManager->flush();

                        return $this->redirectToRoute('admin_recipe_index', [], Response::HTTP_SEE_OTHER);
                }

                return $this->renderForm('admin/recipe/new.html.twig', [
                            'recipe' => $recipe,
                            'form' => $form,
                ]);
        }

        #[Route('/{id}', name: 'admin_recipe_show', methods: ['GET'])]
        public function show(Recipe $recipe): Response
        {
                return $this->render('admin/recipe/show.html.twig', [
                            'recipe' => $recipe,
                ]);
        }

        #[Route('/{id}/edit', name: 'admin_recipe_edit', methods: ['GET', 'POST'])]
        public function edit(Request $request, Recipe $recipe, EntityManagerInterface $entityManager): Response
        {
                $form = $this->createForm(RecipeType::class, $recipe);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->flush();

                        return $this->redirectToRoute('admin_recipe_index', [], Response::HTTP_SEE_OTHER);
                }

                return $this->renderForm('admin/recipe/edit.html.twig', [
                            'recipe' => $recipe,
                            'form' => $form,
                ]);
        }

        #[Route('/{id}', name: 'admin_recipe_delete', methods: ['POST'])]
        public function delete(Request $request, Recipe $recipe, EntityManagerInterface $entityManager): Response
        {
                if ($this->isCsrfTokenValid('delete' . $recipe->getId(), $request->request->get('_token'))) {
                        $entityManager->remove($recipe);
                        $entityManager->flush();
                }

                return $this->redirectToRoute('admin_recipe_index', [], Response::HTTP_SEE_OTHER);
        }
}
