<?php

namespace App\Controller;

use App\Entity\SearchData;
use App\Form\SearchType;
use App\Repository\RecipeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
        private RecipeRepository $recipeRepository;

        /**
         * @param RecipeRepository $recipeRepository
         */
        public function __construct(RecipeRepository $recipeRepository)
        {
                $this->recipeRepository = $recipeRepository;
        }

        #[Route('/search', name: 'search')]
        public function search(RecipeRepository $recipeRepository, Request $request): Response
        {
                $data = new SearchData();
                $form = $this->createForm(SearchType::class, $data);
                $form->handleRequest($request);
                $recipes = $recipeRepository->findSearch($data);
                return $this->render('recette/search.html.twig', [
                            'recipes' => $recipes,
                            'searchForm' => $form->createView()
                ]);
        }
}
