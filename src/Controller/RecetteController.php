<?php

namespace App\Controller;

use App\Form\SearchType;
use App\Repository\IngredientRepository;
use App\Repository\RecipeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecetteController extends AbstractController
{
    private IngredientRepository $ingredientRepository;
    private RecipeRepository $recipeRepository;

    /**
     * @param IngredientRepository $ingredientRepository
     * @param RecipeRepository $recipeRepository
     */
    public function __construct(IngredientRepository $ingredientRepository, RecipeRepository $recipeRepository)
    {
        $this->ingredientRepository = $ingredientRepository;
        $this->recipeRepository = $recipeRepository;
    }

    #[Route('/recettes', name: 'recettes')]
    public function index(Request $request): Response
    {

        $recipeEntities = $this->recipeRepository->findAll();

        return $this->render('recette/recipeList.html.twig', [
            'recipes' => $recipeEntities,
        ]);
    }

    #[Route('/recette/{id}', name: 'recette')]
    public function recipe($id): Response
    {
        $recipeEntity = $this->recipeRepository->find($id);

        return $this->render('recette/recipe.html.twig', [
            'recipe' => $recipeEntity,
        ]);
    }

    #[Route('/recette_chosed', name: 'recette_chosed')]
    public function chosedRecette(Request $request): Response
    {

        $ingredientsId = json_decode($request->query->get('ingredientId'));

//        $ingredientEntities = [];

//        foreach ($idDecodeds as $id) {
//            $ingredientEntity = $this->ingredientRepository->find($id);
//            if ($ingredientEntity !== null) {
//                $ingredientEntities[] = $ingredientEntity->getId();
//            }
//        }

        $sortedByIngredients = true;
        $recipes = $this->recipeRepository->findRecipesByIngredients($ingredientsId);
        $otherRecipes = $this->recipeRepository->findOtherRecipesByIngredients($ingredientsId, $recipes);

        if (empty($recipes)) {
            $recipes = $this->recipeRepository->findAll();
            $sortedByIngredients = false;
        }
        if (count($otherRecipes) === 0 ){
            $otherRecipes = $this->recipeRepository->findOtherRecipesByRecipes($recipes);
        }

        return $this->render('recette_chosed/index.html.twig', [
            'controller_name' => 'RecetteController',
            'recipes' => $recipes,
            'sortedByIngredients' => $sortedByIngredients,
            'otherRecipes' => $otherRecipes,
        ]);
    }
}
