<?php

namespace App\Controller;

use App\Form\SearchRecipeType;
use App\Repository\CategoryRepository;
use App\Repository\IngredientRepository;
use App\Repository\RecipeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IngredientsController extends AbstractController
{

        private RecipeRepository $recipeRepository;
        private IngredientRepository $ingredientRepository;
        private CategoryRepository $categoryRepository;

        /**
         * @param RecipeRepository $recipeRepository
         * @param IngredientRepository $ingredientRepository
         * @param CategoryRepository $categoryRepository
         */
        public function __construct(RecipeRepository $recipeRepository, IngredientRepository $ingredientRepository, CategoryRepository $categoryRepository)
        {
                $this->recipeRepository = $recipeRepository;
                $this->ingredientRepository = $ingredientRepository;
                $this->categoryRepository = $categoryRepository;
        }


        #[Route('/ingredients', name: 'ingredients')]
        public function index(RecipeRepository $recipeRepository, IngredientRepository $ingredientRepository, CategoryRepository $categoryRepository, Request $request): Response
        {
                $qbRecipes = $recipeRepository->getQbAll();
                $qbIngredients = $ingredientRepository->getQbAll();
                $qbCategories = $categoryRepository->getQbAll();
//        $form = $this->createForm(SearchRecipeType::class);
//        $form->handleRequest($request);


//        if ($form->isSubmitted() && $form->isValid()) {
//            $qbRecipes = $this->recipeRepository->findRecipeByIngredients($qbRecipes, $form->getData());
//        }

                $ingredients = $qbIngredients->getQuery()->getResult();
                $categories = $qbCategories->getQuery()->getResult();
                $recipes = $qbRecipes->getQuery()->getResult();

                return $this->render('ingredients/ingredients.html.twig', [
                            'controller_name' => 'IngredientsController',
                            'ingredients' => $ingredients,
                            'categories' => $categories,
                            'recipes' => $recipes
                ]);
        }
}
