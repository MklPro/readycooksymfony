<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @UniqueEntity(fields={"mail"}, message="There is already an account with this mail")
 */
#[
ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['mail', "accountName"], message: 'There is already an account with this username')]

class User implements UserInterface, PasswordAuthenticatedUserInterface
{

        #[ORM\Id]
        #[ORM\GeneratedValue]
        #[ORM\Column(type: 'integer')]
        private $id;

        #[ORM\Column(type: 'json')]
        private $roles = [];

        #[ORM\Column(type: 'string', length: 180, unique: true)]
        private $accountName;

        #[ORM\Column(type: 'string', length: 180)]
        private $nickName;

        #[ORM\Column(type: 'string', length: 180, unique: true)]
        private $mail;

        #[ORM\Column(type: 'string', length: 255)]
        private $password;

        #[ORM\Column(type: 'decimal', precision: 10, scale: '0', nullable: true)]
        private $latitude;

        #[ORM\Column(type: 'decimal', precision: 10, scale: '0', nullable: true)]
        private $longitude;

        #[ORM\Column(type: 'string', length: 255, nullable: true)]
        private $pathImage;

        #[ORM\OneToMany(mappedBy: 'user', targetEntity: Note::class)]
        private $notes;

        #[ORM\ManyToMany(targetEntity: Recipe::class, mappedBy: 'users')]
        private $recipes;

        #[ORM\ManyToMany(targetEntity: Shop::class, inversedBy: 'users')]
        private $shops;

        #[ORM\ManyToMany(targetEntity: Allergy::class, inversedBy: 'users')]
        private $allergies;

        #[ORM\ManyToMany(targetEntity: Diet::class, inversedBy: 'users')]
        private $diets;

        #[ORM\OneToMany(mappedBy: 'user', targetEntity: Comment::class)]
        private $comments;

        public function __construct()
        {
                $this->notes = new ArrayCollection();
                $this->recipes = new ArrayCollection();
                $this->shops = new ArrayCollection();
                $this->allergies = new ArrayCollection();
                $this->diets = new ArrayCollection();
                $this->comments = new ArrayCollection();
        }

        public function getId(): ?int
        {
                return $this->id;
        }

        public function getAccountName(): ?string
        {
                return $this->accountName;
        }

        public function setAccountName(string $accountName): self
        {
                $this->accountName = $accountName;

                return $this;
        }

        public function getNickName(): ?string
        {
                return $this->nickName;
        }

        public function setNickName(string $nickName): self
        {
                $this->nickName = $nickName;

                return $this;
        }

        public function getMail(): ?string
        {
                return $this->mail;
        }

        public function setMail(string $mail): self
        {
                $this->mail = $mail;

                return $this;
        }

        public function getPassword(): ?string
        {
                return $this->password;
        }

        public function setPassword(string $password): self
        {
                $this->password = $password;

                return $this;
        }

        public function getLatitude(): ?string
        {
                return $this->latitude;
        }

        public function setLatitude(?string $latitude): self
        {
                $this->latitude = $latitude;

                return $this;
        }

        public function getLongitude(): ?string
        {
                return $this->longitude;
        }

        public function setLongitude(?string $longitude): self
        {
                $this->longitude = $longitude;

                return $this;
        }

        public function getPathImage(): ?string
        {
                return $this->pathImage;
        }

        public function setPathImage(string $pathImage): self
        {
                $this->pathImage = $pathImage;

                return $this;
        }

        /**
         * @return Collection|Note[]
         */
        public function getNotes(): Collection
        {
                return $this->notes;
        }

        public function addNote(Note $note): self
        {
                if (!$this->notes->contains($note)) {
                        $this->notes[] = $note;
                        $note->setUser($this);
                }

                return $this;
        }

        public function removeNote(Note $note): self
        {
                if ($this->notes->removeElement($note)) {
                        // set the owning side to null (unless already changed)
                        if ($note->getUser() === $this) {
                                $note->setUser(null);
                        }
                }

                return $this;
        }

        /**
         * @return Collection|Recipe[]
         */
        public function getRecipes(): Collection
        {
                return $this->recipes;
        }

        public function addRecipe(Recipe $recipe): self
        {
                if (!$this->recipes->contains($recipe)) {
                        $this->recipes[] = $recipe;
                        $recipe->addUser($this);
                }

                return $this;
        }

        public function removeRecipe(Recipe $recipe): self
        {
                if ($this->recipes->removeElement($recipe)) {
                        $recipe->removeUser($this);
                }

                return $this;
        }

        /**
         * @return Collection|Shop[]
         */
        public function getShops(): Collection
        {
                return $this->shops;
        }

        public function addShop(Shop $shop): self
        {
                if (!$this->shops->contains($shop)) {
                        $this->shops[] = $shop;
                }

                return $this;
        }

        public function removeShop(Shop $shop): self
        {
                $this->shops->removeElement($shop);

                return $this;
        }

        /**
         * @return Collection|Allergy[]
         */
        public function getAllergies(): Collection
        {
                return $this->allergies;
        }

        public function addAllergy(Allergy $allergy): self
        {
                if (!$this->allergies->contains($allergy)) {
                        $this->allergies[] = $allergy;
                }

                return $this;
        }

        public function removeAllergy(Allergy $allergy): self
        {
                $this->allergies->removeElement($allergy);

                return $this;
        }

        /**
         * @return Collection|diet[]
         */
        public function getDiets(): Collection
        {
                return $this->diets;
        }

        public function addDiet(diet $diet): self
        {
                if (!$this->diets->contains($diet)) {
                        $this->diets[] = $diet;
                }

                return $this;
        }

        public function removeDiet(diet $diet): self
        {
                $this->diets->removeElement($diet);

                return $this;
        }

        /**
         * @return Collection|Comment[]
         */
        public function getComments(): Collection
        {
                return $this->comments;
        }

        public function addComment(Comment $comment): self
        {
                if (!$this->comments->contains($comment)) {
                        $this->comments[] = $comment;
                        $comment->setUser($this);
                }

                return $this;
        }

        public function removeComment(Comment $comment): self
        {
                if ($this->comments->removeElement($comment)) {
                        // set the owning side to null (unless already changed)
                        if ($comment->getUser() === $this) {
                                $comment->setUser(null);
                        }
                }

                return $this;
        }

        /**
         * A visual identifier that represents this user.
         *
         * @see UserInterface
         */
        public function getUserIdentifier(): string
        {
                return (string) $this->mail;
        }

        /**
         * @see UserInterface
         */
        public function getRoles(): array
        {
                $roles = $this->roles;
                // guarantee every user at least has ROLE_USER
                $roles[] = 'ROLE_USER';

                return array_unique($roles);
        }

        public function setRoles(array $roles): self
        {
                $this->roles = $roles;

                return $this;
        }

        /**
         * Returning a salt is only needed, if you are not using a modern
         * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
         *
         * @see UserInterface
         */
        public function getSalt(): ?string
        {
                return null;
        }

        /**
         * @see UserInterface
         */
        public function eraseCredentials()
        {
                // If you store any temporary, sensitive data on the user, clear it here
                // $this->plainPassword = null;
        }

        public function getUsername()
        {
                return $this->getNickName();
        }
}
