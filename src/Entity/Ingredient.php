<?php

namespace App\Entity;

use App\Repository\IngredientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: IngredientRepository::class)]
class Ingredient
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToMany(targetEntity: Allergy::class, inversedBy: 'ingredients')]
    private $allergies;

    #[ORM\ManyToMany(targetEntity: Diet::class, inversedBy: 'ingredients')]
    private $diets;

    #[ORM\ManyToOne(targetEntity: Category::class,  inversedBy: 'ingredients')]
    private $category;

    #[ORM\ManyToMany(targetEntity: Recipe::class, mappedBy: 'ingredients', cascade: ['all'])]
    private $recipes;

    public function __construct()
    {
        $this->allergies = new ArrayCollection();
        $this->diets = new ArrayCollection();
        $this->recipes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Allergy[]
     */
    public function getAllergies(): Collection
    {
        return $this->allergies;
    }

    public function addAllergy(Allergy $allergy): self
    {
        if (!$this->allergies->contains($allergy)) {
            $this->allergies[] = $allergy;
        }

        return $this;
    }

    public function removeAllergy(Allergy $allergy): self
    {
        $this->allergies->removeElement($allergy);

        return $this;
    }

    /**
     * @return Collection|diet[]
     */
    public function getDiets(): Collection
    {
        return $this->diets;
    }

    public function addDiet(diet $diet): self
    {
        if (!$this->diets->contains($diet)) {
            $this->diets[] = $diet;
        }

        return $this;
    }

    public function removeDiet(diet $diet): self
    {
        $this->diets->removeElement($diet);

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Recipe[]
     */
    public function getRecipes(): Collection
    {
        return $this->recipes;
    }

    public function addRecipe(Recipe $recipe): self
    {
        if (!$this->recipes->contains($recipe)) {
            $this->recipes[] = $recipe;
            $recipe->addIngredient($this);
        }

        return $this;
    }

    public function removerecipe(Recipe $recipe): self
    {
        $this->recipes->removeElement($recipe);

        return $this;
    }
}
