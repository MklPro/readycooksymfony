<?php

namespace App\Entity;

class SearchData
{
        public ?string $q = '';

        public array  $allergies = [];

        public array $diets = [];

        public array $difficulty = [];

        public array $price = [];

        public int $duration = 0;

        /**
         * @return string|null
         */
        public function getQ(): ?string
        {
                return $this->q;
        }

        /**
         * @param string|null $q
         */
        public function setQ(?string $q): void
        {
                $this->q = $q;
        }

        /**
         * @return array
         */
        public function getAllergies(): array
        {
                return $this->allergies;
        }

        /**
         * @param array $allergies
         */
        public function setAllergies(array $allergies): void
        {
                $this->allergies = $allergies;
        }

        /**
         * @return array
         */
        public function getDiets(): array
        {
                return $this->diets;
        }

        /**
         * @param array $diets
         */
        public function setDiets(array $diets): void
        {
                $this->diets = $diets;
        }

        /**
         * @return array
         */
        public function getDifficulty(): array
        {
                return $this->difficulty;
        }

        /**
         * @param array $difficulty
         */
        public function setDifficulty(array $difficulty): void
        {
                $this->difficulty = $difficulty;
        }

        /**
         * @return array
         */
        public function getPrice(): array
        {
                return $this->price;
        }

        /**
         * @param array $price
         */
        public function setPrice(array $price): void
        {
                $this->price = $price;
        }

        /**
         * @return int
         */
        public function getDuration(): int
        {
                return $this->duration;
        }

        /**
         * @param int $duration
         */
        public function setDuration(int $duration): void
        {
                $this->duration = $duration;
        }



}